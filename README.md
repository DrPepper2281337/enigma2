Программа предназначена для шифровки и расшифровки текста по шифру Виженера.
Функционал:
1) Кнопка "Файл" при нажатии на неё, откроется выпадающие меню на котором находиться:
	а) Кнопка "Открыть", открывает диалоговое окно, где нужно выбрать текстовый файл для открытия.
	б) Кнопка "Сохранить", которая открывает диалоговое окно, где нужно выбрать место для сохранения.
2) Поле "Текст", которое отвечает за исходный текст.
3) Поле "Ключ", которое отвечает за ключ.
4) Два чек бокаса "Зашифровать" "Расшифровать", которые отвечают за переключения режимов работы программы на Зашифровка текста и Расшифровка текста.
5) Кнопка "Рассчитать", которая отвечает за преобразования текста по выбранному режиму.
6) Поле "Преобразованный текст", которое отвечает за вывод преобразованного текста.
