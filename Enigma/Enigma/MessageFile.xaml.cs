﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Enigma
{
    /// <summary>
    /// Логика взаимодействия для MessageFile.xaml
    /// </summary>
    public partial class MessageFile : Window
    {
        public MessageFile()
        {
            InitializeComponent();
        }
        DefaultFileDialog fileDialog = new DefaultFileDialog();
        private void BntEncrypted_Click(object sender, RoutedEventArgs e)
        {
            if (!(MainWindow.EncryptedMessage == "" || MainWindow.EncryptedMessage == null))
            {
                fileDialog.SaveFile(MainWindow.EncryptedMessage, fileDialog.PathFileGet());
                this.Close();
            }
            else
            {
                MessageBox.Show("Поле \"Tекст\" пустое!");
            }
        }

        private void BtnDecrypted_Click(object sender, RoutedEventArgs e)
        {
            if (!(MainWindow.DecryptedMessage == "" || MainWindow.DecryptedMessage == null))
            {
                fileDialog.SaveFile(MainWindow.DecryptedMessage, fileDialog.PathFileGet());
                this.Close();
            }
            else
            {
                MessageBox.Show("Поле \"Преобразованный текст\" пустое!");
            }
        }
    }
}
