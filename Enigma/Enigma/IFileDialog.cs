﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma
{
    interface IFileDialog
    {
        string Path { get; set; }
        string OpenFile(string path);
        bool SaveFile(string text, string path);
    }
}
